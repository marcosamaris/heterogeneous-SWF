# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1/spack-build/testing/testing_cauxiliary.c" "/tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1/spack-build/testing/CMakeFiles/ctesting.dir/testing_cauxiliary.c.o"
  "/tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1/spack-build/testing/testing_cgels.c" "/tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1/spack-build/testing/CMakeFiles/ctesting.dir/testing_cgels.c.o"
  "/tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1/spack-build/testing/testing_cgemm.c" "/tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1/spack-build/testing/CMakeFiles/ctesting.dir/testing_cgemm.c.o"
  "/tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1/spack-build/testing/testing_cgesv_incpiv.c" "/tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1/spack-build/testing/CMakeFiles/ctesting.dir/testing_cgesv_incpiv.c.o"
  "/tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1/spack-build/testing/testing_chemm.c" "/tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1/spack-build/testing/CMakeFiles/ctesting.dir/testing_chemm.c.o"
  "/tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1/spack-build/testing/testing_cher2k.c" "/tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1/spack-build/testing/CMakeFiles/ctesting.dir/testing_cher2k.c.o"
  "/tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1/spack-build/testing/testing_cherk.c" "/tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1/spack-build/testing/CMakeFiles/ctesting.dir/testing_cherk.c.o"
  "/tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1/spack-build/testing/testing_clange.c" "/tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1/spack-build/testing/CMakeFiles/ctesting.dir/testing_clange.c.o"
  "/tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1/spack-build/testing/testing_cpemv.c" "/tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1/spack-build/testing/CMakeFiles/ctesting.dir/testing_cpemv.c.o"
  "/tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1/spack-build/testing/testing_cposv.c" "/tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1/spack-build/testing/CMakeFiles/ctesting.dir/testing_cposv.c.o"
  "/tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1/spack-build/testing/testing_cpotri.c" "/tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1/spack-build/testing/CMakeFiles/ctesting.dir/testing_cpotri.c.o"
  "/tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1/spack-build/testing/testing_csymm.c" "/tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1/spack-build/testing/CMakeFiles/ctesting.dir/testing_csymm.c.o"
  "/tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1/spack-build/testing/testing_csyr2k.c" "/tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1/spack-build/testing/CMakeFiles/ctesting.dir/testing_csyr2k.c.o"
  "/tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1/spack-build/testing/testing_csyrk.c" "/tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1/spack-build/testing/CMakeFiles/ctesting.dir/testing_csyrk.c.o"
  "/tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1/spack-build/testing/testing_ctrmm.c" "/tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1/spack-build/testing/CMakeFiles/ctesting.dir/testing_ctrmm.c.o"
  "/tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1/spack-build/testing/testing_ctrsm.c" "/tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1/spack-build/testing/CMakeFiles/ctesting.dir/testing_ctrsm.c.o"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "ADD_"
  "CHAMELEON_SCHED_STARPU"
  "CHAMELEON_USE_MPI"
  "CHAMELEON_USE_STARPU_DATA_WONT_USE"
  "CHAMELEON_USE_STARPU_IDLE_PREFETCH"
  "HAVE_STARPU_MPI_DATA_REGISTER"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "/home/marcos/chameleon/spack/opt/spack/linux-x86_64/gcc-4.8.5/openmpi-2.0.1-wmrycolmrlgieebzmb7vbndworuorggr/include"
  "/home/marcos/chameleon/spack/opt/spack/linux-x86_64/gcc-4.8.5/starpu-1.2.1-b3ym3ekzbvawzmi5ft7u4wsoomckww6p/include/starpu/1.2"
  "../"
  "."
  "../include"
  "include"
  "../testing"
  "testing"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1/spack-build/compute/CMakeFiles/chameleon.dir/DependInfo.cmake"
  "/tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1/spack-build/runtime/starpu/CMakeFiles/chameleon_starpu.dir/DependInfo.cmake"
  "/tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1/spack-build/coreblas/compute/CMakeFiles/coreblas.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
