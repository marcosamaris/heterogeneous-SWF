# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1/spack-build/testing/testing_zauxiliary.c" "/tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1/spack-build/testing/CMakeFiles/ztesting.dir/testing_zauxiliary.c.o"
  "/tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1/spack-build/testing/testing_zgels.c" "/tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1/spack-build/testing/CMakeFiles/ztesting.dir/testing_zgels.c.o"
  "/tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1/spack-build/testing/testing_zgemm.c" "/tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1/spack-build/testing/CMakeFiles/ztesting.dir/testing_zgemm.c.o"
  "/tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1/spack-build/testing/testing_zgesv_incpiv.c" "/tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1/spack-build/testing/CMakeFiles/ztesting.dir/testing_zgesv_incpiv.c.o"
  "/tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1/spack-build/testing/testing_zhemm.c" "/tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1/spack-build/testing/CMakeFiles/ztesting.dir/testing_zhemm.c.o"
  "/tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1/spack-build/testing/testing_zher2k.c" "/tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1/spack-build/testing/CMakeFiles/ztesting.dir/testing_zher2k.c.o"
  "/tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1/spack-build/testing/testing_zherk.c" "/tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1/spack-build/testing/CMakeFiles/ztesting.dir/testing_zherk.c.o"
  "/tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1/spack-build/testing/testing_zlange.c" "/tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1/spack-build/testing/CMakeFiles/ztesting.dir/testing_zlange.c.o"
  "/tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1/spack-build/testing/testing_zpemv.c" "/tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1/spack-build/testing/CMakeFiles/ztesting.dir/testing_zpemv.c.o"
  "/tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1/spack-build/testing/testing_zposv.c" "/tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1/spack-build/testing/CMakeFiles/ztesting.dir/testing_zposv.c.o"
  "/tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1/spack-build/testing/testing_zpotri.c" "/tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1/spack-build/testing/CMakeFiles/ztesting.dir/testing_zpotri.c.o"
  "/tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1/spack-build/testing/testing_zsymm.c" "/tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1/spack-build/testing/CMakeFiles/ztesting.dir/testing_zsymm.c.o"
  "/tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1/spack-build/testing/testing_zsyr2k.c" "/tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1/spack-build/testing/CMakeFiles/ztesting.dir/testing_zsyr2k.c.o"
  "/tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1/spack-build/testing/testing_zsyrk.c" "/tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1/spack-build/testing/CMakeFiles/ztesting.dir/testing_zsyrk.c.o"
  "/tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1/spack-build/testing/testing_ztrmm.c" "/tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1/spack-build/testing/CMakeFiles/ztesting.dir/testing_ztrmm.c.o"
  "/tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1/spack-build/testing/testing_ztrsm.c" "/tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1/spack-build/testing/CMakeFiles/ztesting.dir/testing_ztrsm.c.o"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "ADD_"
  "CHAMELEON_SCHED_STARPU"
  "CHAMELEON_USE_MPI"
  "CHAMELEON_USE_STARPU_DATA_WONT_USE"
  "CHAMELEON_USE_STARPU_IDLE_PREFETCH"
  "HAVE_STARPU_MPI_DATA_REGISTER"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "/home/marcos/chameleon/spack/opt/spack/linux-x86_64/gcc-4.8.5/openmpi-2.0.1-wmrycolmrlgieebzmb7vbndworuorggr/include"
  "/home/marcos/chameleon/spack/opt/spack/linux-x86_64/gcc-4.8.5/starpu-1.2.1-b3ym3ekzbvawzmi5ft7u4wsoomckww6p/include/starpu/1.2"
  "../"
  "."
  "../include"
  "include"
  "../testing"
  "testing"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1/spack-build/compute/CMakeFiles/chameleon.dir/DependInfo.cmake"
  "/tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1/spack-build/runtime/starpu/CMakeFiles/chameleon_starpu.dir/DependInfo.cmake"
  "/tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1/spack-build/coreblas/compute/CMakeFiles/coreblas.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
