# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1/spack-build/testing/testing_sauxiliary.c" "/tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1/spack-build/testing/CMakeFiles/stesting.dir/testing_sauxiliary.c.o"
  "/tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1/spack-build/testing/testing_sgels.c" "/tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1/spack-build/testing/CMakeFiles/stesting.dir/testing_sgels.c.o"
  "/tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1/spack-build/testing/testing_sgemm.c" "/tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1/spack-build/testing/CMakeFiles/stesting.dir/testing_sgemm.c.o"
  "/tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1/spack-build/testing/testing_sgesv_incpiv.c" "/tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1/spack-build/testing/CMakeFiles/stesting.dir/testing_sgesv_incpiv.c.o"
  "/tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1/spack-build/testing/testing_slange.c" "/tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1/spack-build/testing/CMakeFiles/stesting.dir/testing_slange.c.o"
  "/tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1/spack-build/testing/testing_spemv.c" "/tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1/spack-build/testing/CMakeFiles/stesting.dir/testing_spemv.c.o"
  "/tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1/spack-build/testing/testing_sposv.c" "/tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1/spack-build/testing/CMakeFiles/stesting.dir/testing_sposv.c.o"
  "/tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1/spack-build/testing/testing_spotri.c" "/tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1/spack-build/testing/CMakeFiles/stesting.dir/testing_spotri.c.o"
  "/tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1/spack-build/testing/testing_ssymm.c" "/tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1/spack-build/testing/CMakeFiles/stesting.dir/testing_ssymm.c.o"
  "/tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1/spack-build/testing/testing_ssyr2k.c" "/tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1/spack-build/testing/CMakeFiles/stesting.dir/testing_ssyr2k.c.o"
  "/tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1/spack-build/testing/testing_ssyrk.c" "/tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1/spack-build/testing/CMakeFiles/stesting.dir/testing_ssyrk.c.o"
  "/tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1/spack-build/testing/testing_strmm.c" "/tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1/spack-build/testing/CMakeFiles/stesting.dir/testing_strmm.c.o"
  "/tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1/spack-build/testing/testing_strsm.c" "/tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1/spack-build/testing/CMakeFiles/stesting.dir/testing_strsm.c.o"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "ADD_"
  "CHAMELEON_SCHED_STARPU"
  "CHAMELEON_USE_MPI"
  "CHAMELEON_USE_STARPU_DATA_WONT_USE"
  "CHAMELEON_USE_STARPU_IDLE_PREFETCH"
  "HAVE_STARPU_MPI_DATA_REGISTER"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "/home/marcos/chameleon/spack/opt/spack/linux-x86_64/gcc-4.8.5/openmpi-2.0.1-wmrycolmrlgieebzmb7vbndworuorggr/include"
  "/home/marcos/chameleon/spack/opt/spack/linux-x86_64/gcc-4.8.5/starpu-1.2.1-b3ym3ekzbvawzmi5ft7u4wsoomckww6p/include/starpu/1.2"
  "../"
  "."
  "../include"
  "include"
  "../testing"
  "testing"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1/spack-build/compute/CMakeFiles/chameleon.dir/DependInfo.cmake"
  "/tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1/spack-build/runtime/starpu/CMakeFiles/chameleon_starpu.dir/DependInfo.cmake"
  "/tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1/spack-build/coreblas/compute/CMakeFiles/coreblas.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
