file(REMOVE_RECURSE
  "pcgemm.c"
  "psgemm.c"
  "pzgemm.c"
  "pdgemm.c"
  "pchemm.c"
  "pzhemm.c"
  "pcher2k.c"
  "pzher2k.c"
  "pcherk.c"
  "pzherk.c"
  "pcsymm.c"
  "pssymm.c"
  "pzsymm.c"
  "pdsymm.c"
  "pcsyr2k.c"
  "pssyr2k.c"
  "pzsyr2k.c"
  "pdsyr2k.c"
  "pcsyrk.c"
  "pssyrk.c"
  "pzsyrk.c"
  "pdsyrk.c"
  "pctrmm.c"
  "pstrmm.c"
  "pztrmm.c"
  "pdtrmm.c"
  "pctrsm.c"
  "pstrsm.c"
  "pztrsm.c"
  "pdtrsm.c"
  "pctrsmpl.c"
  "pstrsmpl.c"
  "pztrsmpl.c"
  "pdtrsmpl.c"
  "cgemm.c"
  "sgemm.c"
  "zgemm.c"
  "dgemm.c"
  "chemm.c"
  "zhemm.c"
  "cher2k.c"
  "zher2k.c"
  "cherk.c"
  "zherk.c"
  "csymm.c"
  "ssymm.c"
  "zsymm.c"
  "dsymm.c"
  "csyr2k.c"
  "ssyr2k.c"
  "zsyr2k.c"
  "dsyr2k.c"
  "csyrk.c"
  "ssyrk.c"
  "zsyrk.c"
  "dsyrk.c"
  "ctrmm.c"
  "strmm.c"
  "ztrmm.c"
  "dtrmm.c"
  "ctrsm.c"
  "strsm.c"
  "ztrsm.c"
  "dtrsm.c"
  "ctrsmpl.c"
  "strsmpl.c"
  "ztrsmpl.c"
  "dtrsmpl.c"
  "pcgeadd.c"
  "psgeadd.c"
  "pzgeadd.c"
  "pdgeadd.c"
  "pcgelqf.c"
  "psgelqf.c"
  "pzgelqf.c"
  "pdgelqf.c"
  "pcgelqfrh.c"
  "psgelqfrh.c"
  "pzgelqfrh.c"
  "pdgelqfrh.c"
  "pcgeqrf.c"
  "psgeqrf.c"
  "pzgeqrf.c"
  "pdgeqrf.c"
  "pcgeqrfrh.c"
  "psgeqrfrh.c"
  "pzgeqrfrh.c"
  "pdgeqrfrh.c"
  "pcgetrf_incpiv.c"
  "psgetrf_incpiv.c"
  "pzgetrf_incpiv.c"
  "pdgetrf_incpiv.c"
  "pcgetrf_nopiv.c"
  "psgetrf_nopiv.c"
  "pzgetrf_nopiv.c"
  "pdgetrf_nopiv.c"
  "pclacpy.c"
  "pslacpy.c"
  "pzlacpy.c"
  "pdlacpy.c"
  "pclange.c"
  "pslange.c"
  "pzlange.c"
  "pdlange.c"
  "pclanhe.c"
  "pzlanhe.c"
  "pclansy.c"
  "pslansy.c"
  "pzlansy.c"
  "pdlansy.c"
  "pclantr.c"
  "pslantr.c"
  "pzlantr.c"
  "pdlantr.c"
  "pclaset2.c"
  "pslaset2.c"
  "pzlaset2.c"
  "pdlaset2.c"
  "pclaset.c"
  "pslaset.c"
  "pzlaset.c"
  "pdlaset.c"
  "pclauum.c"
  "pslauum.c"
  "pzlauum.c"
  "pdlauum.c"
  "pcplghe.c"
  "pzplghe.c"
  "pcplgsy.c"
  "psplgsy.c"
  "pzplgsy.c"
  "pdplgsy.c"
  "pcplrnt.c"
  "psplrnt.c"
  "pzplrnt.c"
  "pdplrnt.c"
  "pcpotrf.c"
  "pspotrf.c"
  "pzpotrf.c"
  "pdpotrf.c"
  "pcsytrf.c"
  "pzsytrf.c"
  "pctrtri.c"
  "pstrtri.c"
  "pztrtri.c"
  "pdtrtri.c"
  "pcpotrimm.c"
  "pspotrimm.c"
  "pzpotrimm.c"
  "pdpotrimm.c"
  "pcunglq.c"
  "psorglq.c"
  "pzunglq.c"
  "pdorglq.c"
  "pcunglqrh.c"
  "psorglqrh.c"
  "pzunglqrh.c"
  "pdorglqrh.c"
  "pcungqr.c"
  "psorgqr.c"
  "pzungqr.c"
  "pdorgqr.c"
  "pcungqrrh.c"
  "psorgqrrh.c"
  "pzungqrrh.c"
  "pdorgqrrh.c"
  "pcunmlq.c"
  "psormlq.c"
  "pzunmlq.c"
  "pdormlq.c"
  "pcunmlqrh.c"
  "psormlqrh.c"
  "pzunmlqrh.c"
  "pdormlqrh.c"
  "pcunmqr.c"
  "psormqr.c"
  "pzunmqr.c"
  "pdormqr.c"
  "pcunmqrrh.c"
  "psormqrrh.c"
  "pzunmqrrh.c"
  "pdormqrrh.c"
  "cgels.c"
  "sgels.c"
  "zgels.c"
  "dgels.c"
  "cgelqf.c"
  "sgelqf.c"
  "zgelqf.c"
  "dgelqf.c"
  "cgelqs.c"
  "sgelqs.c"
  "zgelqs.c"
  "dgelqs.c"
  "cgeqrf.c"
  "sgeqrf.c"
  "zgeqrf.c"
  "dgeqrf.c"
  "cgeqrs.c"
  "sgeqrs.c"
  "zgeqrs.c"
  "dgeqrs.c"
  "cgesv_incpiv.c"
  "sgesv_incpiv.c"
  "zgesv_incpiv.c"
  "dgesv_incpiv.c"
  "cgesv_nopiv.c"
  "sgesv_nopiv.c"
  "zgesv_nopiv.c"
  "dgesv_nopiv.c"
  "cgetrf_incpiv.c"
  "sgetrf_incpiv.c"
  "zgetrf_incpiv.c"
  "dgetrf_incpiv.c"
  "cgetrf_nopiv.c"
  "sgetrf_nopiv.c"
  "zgetrf_nopiv.c"
  "dgetrf_nopiv.c"
  "cgetrs_incpiv.c"
  "sgetrs_incpiv.c"
  "zgetrs_incpiv.c"
  "dgetrs_incpiv.c"
  "cgetrs_nopiv.c"
  "sgetrs_nopiv.c"
  "zgetrs_nopiv.c"
  "dgetrs_nopiv.c"
  "clacpy.c"
  "slacpy.c"
  "zlacpy.c"
  "dlacpy.c"
  "clange.c"
  "slange.c"
  "zlange.c"
  "dlange.c"
  "clanhe.c"
  "zlanhe.c"
  "clansy.c"
  "slansy.c"
  "zlansy.c"
  "dlansy.c"
  "clantr.c"
  "slantr.c"
  "zlantr.c"
  "dlantr.c"
  "claset.c"
  "slaset.c"
  "zlaset.c"
  "dlaset.c"
  "clauum.c"
  "slauum.c"
  "zlauum.c"
  "dlauum.c"
  "cplghe.c"
  "zplghe.c"
  "cplgsy.c"
  "splgsy.c"
  "zplgsy.c"
  "dplgsy.c"
  "cplrnt.c"
  "splrnt.c"
  "zplrnt.c"
  "dplrnt.c"
  "cposv.c"
  "sposv.c"
  "zposv.c"
  "dposv.c"
  "csysv.c"
  "zsysv.c"
  "cpotrf.c"
  "spotrf.c"
  "zpotrf.c"
  "dpotrf.c"
  "csytrf.c"
  "zsytrf.c"
  "cpotri.c"
  "spotri.c"
  "zpotri.c"
  "dpotri.c"
  "cpotrimm.c"
  "spotrimm.c"
  "zpotrimm.c"
  "dpotrimm.c"
  "cpotrs.c"
  "spotrs.c"
  "zpotrs.c"
  "dpotrs.c"
  "csytrs.c"
  "zsytrs.c"
  "ctrtri.c"
  "strtri.c"
  "ztrtri.c"
  "dtrtri.c"
  "cunglq.c"
  "sorglq.c"
  "zunglq.c"
  "dorglq.c"
  "cungqr.c"
  "sorgqr.c"
  "zungqr.c"
  "dorgqr.c"
  "cunmlq.c"
  "sormlq.c"
  "zunmlq.c"
  "dormlq.c"
  "cunmqr.c"
  "sormqr.c"
  "zunmqr.c"
  "dormqr.c"
  "pctile.c"
  "pstile.c"
  "pztile.c"
  "pdtile.c"
  "ctile.c"
  "stile.c"
  "ztile.c"
  "dtile.c"
  "control/workspace_c.c"
  "control/workspace_s.c"
  "control/workspace_z.c"
  "control/workspace_d.c"
  "control/morse_cf77.c"
  "control/morse_sf77.c"
  "control/morse_zf77.c"
  "control/morse_df77.c"
  "CMakeFiles/chameleon.dir/__/control/async.c.o"
  "CMakeFiles/chameleon.dir/__/control/auxiliary.c.o"
  "CMakeFiles/chameleon.dir/__/control/context.c.o"
  "CMakeFiles/chameleon.dir/__/control/control.c.o"
  "CMakeFiles/chameleon.dir/__/control/descriptor.c.o"
  "CMakeFiles/chameleon.dir/__/control/workspace.c.o"
  "CMakeFiles/chameleon.dir/__/control/tile.c.o"
  "CMakeFiles/chameleon.dir/__/control/morse_f77.c.o"
  "CMakeFiles/chameleon.dir/__/control/morse_mf77.c.o"
  "CMakeFiles/chameleon.dir/pcgemm.c.o"
  "CMakeFiles/chameleon.dir/psgemm.c.o"
  "CMakeFiles/chameleon.dir/pzgemm.c.o"
  "CMakeFiles/chameleon.dir/pdgemm.c.o"
  "CMakeFiles/chameleon.dir/pchemm.c.o"
  "CMakeFiles/chameleon.dir/pzhemm.c.o"
  "CMakeFiles/chameleon.dir/pcher2k.c.o"
  "CMakeFiles/chameleon.dir/pzher2k.c.o"
  "CMakeFiles/chameleon.dir/pcherk.c.o"
  "CMakeFiles/chameleon.dir/pzherk.c.o"
  "CMakeFiles/chameleon.dir/pcsymm.c.o"
  "CMakeFiles/chameleon.dir/pssymm.c.o"
  "CMakeFiles/chameleon.dir/pzsymm.c.o"
  "CMakeFiles/chameleon.dir/pdsymm.c.o"
  "CMakeFiles/chameleon.dir/pcsyr2k.c.o"
  "CMakeFiles/chameleon.dir/pssyr2k.c.o"
  "CMakeFiles/chameleon.dir/pzsyr2k.c.o"
  "CMakeFiles/chameleon.dir/pdsyr2k.c.o"
  "CMakeFiles/chameleon.dir/pcsyrk.c.o"
  "CMakeFiles/chameleon.dir/pssyrk.c.o"
  "CMakeFiles/chameleon.dir/pzsyrk.c.o"
  "CMakeFiles/chameleon.dir/pdsyrk.c.o"
  "CMakeFiles/chameleon.dir/pctrmm.c.o"
  "CMakeFiles/chameleon.dir/pstrmm.c.o"
  "CMakeFiles/chameleon.dir/pztrmm.c.o"
  "CMakeFiles/chameleon.dir/pdtrmm.c.o"
  "CMakeFiles/chameleon.dir/pctrsm.c.o"
  "CMakeFiles/chameleon.dir/pstrsm.c.o"
  "CMakeFiles/chameleon.dir/pztrsm.c.o"
  "CMakeFiles/chameleon.dir/pdtrsm.c.o"
  "CMakeFiles/chameleon.dir/pctrsmpl.c.o"
  "CMakeFiles/chameleon.dir/pstrsmpl.c.o"
  "CMakeFiles/chameleon.dir/pztrsmpl.c.o"
  "CMakeFiles/chameleon.dir/pdtrsmpl.c.o"
  "CMakeFiles/chameleon.dir/cgemm.c.o"
  "CMakeFiles/chameleon.dir/sgemm.c.o"
  "CMakeFiles/chameleon.dir/zgemm.c.o"
  "CMakeFiles/chameleon.dir/dgemm.c.o"
  "CMakeFiles/chameleon.dir/chemm.c.o"
  "CMakeFiles/chameleon.dir/zhemm.c.o"
  "CMakeFiles/chameleon.dir/cher2k.c.o"
  "CMakeFiles/chameleon.dir/zher2k.c.o"
  "CMakeFiles/chameleon.dir/cherk.c.o"
  "CMakeFiles/chameleon.dir/zherk.c.o"
  "CMakeFiles/chameleon.dir/csymm.c.o"
  "CMakeFiles/chameleon.dir/ssymm.c.o"
  "CMakeFiles/chameleon.dir/zsymm.c.o"
  "CMakeFiles/chameleon.dir/dsymm.c.o"
  "CMakeFiles/chameleon.dir/csyr2k.c.o"
  "CMakeFiles/chameleon.dir/ssyr2k.c.o"
  "CMakeFiles/chameleon.dir/zsyr2k.c.o"
  "CMakeFiles/chameleon.dir/dsyr2k.c.o"
  "CMakeFiles/chameleon.dir/csyrk.c.o"
  "CMakeFiles/chameleon.dir/ssyrk.c.o"
  "CMakeFiles/chameleon.dir/zsyrk.c.o"
  "CMakeFiles/chameleon.dir/dsyrk.c.o"
  "CMakeFiles/chameleon.dir/ctrmm.c.o"
  "CMakeFiles/chameleon.dir/strmm.c.o"
  "CMakeFiles/chameleon.dir/ztrmm.c.o"
  "CMakeFiles/chameleon.dir/dtrmm.c.o"
  "CMakeFiles/chameleon.dir/ctrsm.c.o"
  "CMakeFiles/chameleon.dir/strsm.c.o"
  "CMakeFiles/chameleon.dir/ztrsm.c.o"
  "CMakeFiles/chameleon.dir/dtrsm.c.o"
  "CMakeFiles/chameleon.dir/ctrsmpl.c.o"
  "CMakeFiles/chameleon.dir/strsmpl.c.o"
  "CMakeFiles/chameleon.dir/ztrsmpl.c.o"
  "CMakeFiles/chameleon.dir/dtrsmpl.c.o"
  "CMakeFiles/chameleon.dir/pcgeadd.c.o"
  "CMakeFiles/chameleon.dir/psgeadd.c.o"
  "CMakeFiles/chameleon.dir/pzgeadd.c.o"
  "CMakeFiles/chameleon.dir/pdgeadd.c.o"
  "CMakeFiles/chameleon.dir/pcgelqf.c.o"
  "CMakeFiles/chameleon.dir/psgelqf.c.o"
  "CMakeFiles/chameleon.dir/pzgelqf.c.o"
  "CMakeFiles/chameleon.dir/pdgelqf.c.o"
  "CMakeFiles/chameleon.dir/pcgelqfrh.c.o"
  "CMakeFiles/chameleon.dir/psgelqfrh.c.o"
  "CMakeFiles/chameleon.dir/pzgelqfrh.c.o"
  "CMakeFiles/chameleon.dir/pdgelqfrh.c.o"
  "CMakeFiles/chameleon.dir/pcgeqrf.c.o"
  "CMakeFiles/chameleon.dir/psgeqrf.c.o"
  "CMakeFiles/chameleon.dir/pzgeqrf.c.o"
  "CMakeFiles/chameleon.dir/pdgeqrf.c.o"
  "CMakeFiles/chameleon.dir/pcgeqrfrh.c.o"
  "CMakeFiles/chameleon.dir/psgeqrfrh.c.o"
  "CMakeFiles/chameleon.dir/pzgeqrfrh.c.o"
  "CMakeFiles/chameleon.dir/pdgeqrfrh.c.o"
  "CMakeFiles/chameleon.dir/pcgetrf_incpiv.c.o"
  "CMakeFiles/chameleon.dir/psgetrf_incpiv.c.o"
  "CMakeFiles/chameleon.dir/pzgetrf_incpiv.c.o"
  "CMakeFiles/chameleon.dir/pdgetrf_incpiv.c.o"
  "CMakeFiles/chameleon.dir/pcgetrf_nopiv.c.o"
  "CMakeFiles/chameleon.dir/psgetrf_nopiv.c.o"
  "CMakeFiles/chameleon.dir/pzgetrf_nopiv.c.o"
  "CMakeFiles/chameleon.dir/pdgetrf_nopiv.c.o"
  "CMakeFiles/chameleon.dir/pclacpy.c.o"
  "CMakeFiles/chameleon.dir/pslacpy.c.o"
  "CMakeFiles/chameleon.dir/pzlacpy.c.o"
  "CMakeFiles/chameleon.dir/pdlacpy.c.o"
  "CMakeFiles/chameleon.dir/pclange.c.o"
  "CMakeFiles/chameleon.dir/pslange.c.o"
  "CMakeFiles/chameleon.dir/pzlange.c.o"
  "CMakeFiles/chameleon.dir/pdlange.c.o"
  "CMakeFiles/chameleon.dir/pclanhe.c.o"
  "CMakeFiles/chameleon.dir/pzlanhe.c.o"
  "CMakeFiles/chameleon.dir/pclansy.c.o"
  "CMakeFiles/chameleon.dir/pslansy.c.o"
  "CMakeFiles/chameleon.dir/pzlansy.c.o"
  "CMakeFiles/chameleon.dir/pdlansy.c.o"
  "CMakeFiles/chameleon.dir/pclantr.c.o"
  "CMakeFiles/chameleon.dir/pslantr.c.o"
  "CMakeFiles/chameleon.dir/pzlantr.c.o"
  "CMakeFiles/chameleon.dir/pdlantr.c.o"
  "CMakeFiles/chameleon.dir/pclaset2.c.o"
  "CMakeFiles/chameleon.dir/pslaset2.c.o"
  "CMakeFiles/chameleon.dir/pzlaset2.c.o"
  "CMakeFiles/chameleon.dir/pdlaset2.c.o"
  "CMakeFiles/chameleon.dir/pclaset.c.o"
  "CMakeFiles/chameleon.dir/pslaset.c.o"
  "CMakeFiles/chameleon.dir/pzlaset.c.o"
  "CMakeFiles/chameleon.dir/pdlaset.c.o"
  "CMakeFiles/chameleon.dir/pclauum.c.o"
  "CMakeFiles/chameleon.dir/pslauum.c.o"
  "CMakeFiles/chameleon.dir/pzlauum.c.o"
  "CMakeFiles/chameleon.dir/pdlauum.c.o"
  "CMakeFiles/chameleon.dir/pcplghe.c.o"
  "CMakeFiles/chameleon.dir/pzplghe.c.o"
  "CMakeFiles/chameleon.dir/pcplgsy.c.o"
  "CMakeFiles/chameleon.dir/psplgsy.c.o"
  "CMakeFiles/chameleon.dir/pzplgsy.c.o"
  "CMakeFiles/chameleon.dir/pdplgsy.c.o"
  "CMakeFiles/chameleon.dir/pcplrnt.c.o"
  "CMakeFiles/chameleon.dir/psplrnt.c.o"
  "CMakeFiles/chameleon.dir/pzplrnt.c.o"
  "CMakeFiles/chameleon.dir/pdplrnt.c.o"
  "CMakeFiles/chameleon.dir/pcpotrf.c.o"
  "CMakeFiles/chameleon.dir/pspotrf.c.o"
  "CMakeFiles/chameleon.dir/pzpotrf.c.o"
  "CMakeFiles/chameleon.dir/pdpotrf.c.o"
  "CMakeFiles/chameleon.dir/pcsytrf.c.o"
  "CMakeFiles/chameleon.dir/pzsytrf.c.o"
  "CMakeFiles/chameleon.dir/pctrtri.c.o"
  "CMakeFiles/chameleon.dir/pstrtri.c.o"
  "CMakeFiles/chameleon.dir/pztrtri.c.o"
  "CMakeFiles/chameleon.dir/pdtrtri.c.o"
  "CMakeFiles/chameleon.dir/pcpotrimm.c.o"
  "CMakeFiles/chameleon.dir/pspotrimm.c.o"
  "CMakeFiles/chameleon.dir/pzpotrimm.c.o"
  "CMakeFiles/chameleon.dir/pdpotrimm.c.o"
  "CMakeFiles/chameleon.dir/pcunglq.c.o"
  "CMakeFiles/chameleon.dir/psorglq.c.o"
  "CMakeFiles/chameleon.dir/pzunglq.c.o"
  "CMakeFiles/chameleon.dir/pdorglq.c.o"
  "CMakeFiles/chameleon.dir/pcunglqrh.c.o"
  "CMakeFiles/chameleon.dir/psorglqrh.c.o"
  "CMakeFiles/chameleon.dir/pzunglqrh.c.o"
  "CMakeFiles/chameleon.dir/pdorglqrh.c.o"
  "CMakeFiles/chameleon.dir/pcungqr.c.o"
  "CMakeFiles/chameleon.dir/psorgqr.c.o"
  "CMakeFiles/chameleon.dir/pzungqr.c.o"
  "CMakeFiles/chameleon.dir/pdorgqr.c.o"
  "CMakeFiles/chameleon.dir/pcungqrrh.c.o"
  "CMakeFiles/chameleon.dir/psorgqrrh.c.o"
  "CMakeFiles/chameleon.dir/pzungqrrh.c.o"
  "CMakeFiles/chameleon.dir/pdorgqrrh.c.o"
  "CMakeFiles/chameleon.dir/pcunmlq.c.o"
  "CMakeFiles/chameleon.dir/psormlq.c.o"
  "CMakeFiles/chameleon.dir/pzunmlq.c.o"
  "CMakeFiles/chameleon.dir/pdormlq.c.o"
  "CMakeFiles/chameleon.dir/pcunmlqrh.c.o"
  "CMakeFiles/chameleon.dir/psormlqrh.c.o"
  "CMakeFiles/chameleon.dir/pzunmlqrh.c.o"
  "CMakeFiles/chameleon.dir/pdormlqrh.c.o"
  "CMakeFiles/chameleon.dir/pcunmqr.c.o"
  "CMakeFiles/chameleon.dir/psormqr.c.o"
  "CMakeFiles/chameleon.dir/pzunmqr.c.o"
  "CMakeFiles/chameleon.dir/pdormqr.c.o"
  "CMakeFiles/chameleon.dir/pcunmqrrh.c.o"
  "CMakeFiles/chameleon.dir/psormqrrh.c.o"
  "CMakeFiles/chameleon.dir/pzunmqrrh.c.o"
  "CMakeFiles/chameleon.dir/pdormqrrh.c.o"
  "CMakeFiles/chameleon.dir/cgels.c.o"
  "CMakeFiles/chameleon.dir/sgels.c.o"
  "CMakeFiles/chameleon.dir/zgels.c.o"
  "CMakeFiles/chameleon.dir/dgels.c.o"
  "CMakeFiles/chameleon.dir/cgelqf.c.o"
  "CMakeFiles/chameleon.dir/sgelqf.c.o"
  "CMakeFiles/chameleon.dir/zgelqf.c.o"
  "CMakeFiles/chameleon.dir/dgelqf.c.o"
  "CMakeFiles/chameleon.dir/cgelqs.c.o"
  "CMakeFiles/chameleon.dir/sgelqs.c.o"
  "CMakeFiles/chameleon.dir/zgelqs.c.o"
  "CMakeFiles/chameleon.dir/dgelqs.c.o"
  "CMakeFiles/chameleon.dir/cgeqrf.c.o"
  "CMakeFiles/chameleon.dir/sgeqrf.c.o"
  "CMakeFiles/chameleon.dir/zgeqrf.c.o"
  "CMakeFiles/chameleon.dir/dgeqrf.c.o"
  "CMakeFiles/chameleon.dir/cgeqrs.c.o"
  "CMakeFiles/chameleon.dir/sgeqrs.c.o"
  "CMakeFiles/chameleon.dir/zgeqrs.c.o"
  "CMakeFiles/chameleon.dir/dgeqrs.c.o"
  "CMakeFiles/chameleon.dir/cgesv_incpiv.c.o"
  "CMakeFiles/chameleon.dir/sgesv_incpiv.c.o"
  "CMakeFiles/chameleon.dir/zgesv_incpiv.c.o"
  "CMakeFiles/chameleon.dir/dgesv_incpiv.c.o"
  "CMakeFiles/chameleon.dir/cgesv_nopiv.c.o"
  "CMakeFiles/chameleon.dir/sgesv_nopiv.c.o"
  "CMakeFiles/chameleon.dir/zgesv_nopiv.c.o"
  "CMakeFiles/chameleon.dir/dgesv_nopiv.c.o"
  "CMakeFiles/chameleon.dir/cgetrf_incpiv.c.o"
  "CMakeFiles/chameleon.dir/sgetrf_incpiv.c.o"
  "CMakeFiles/chameleon.dir/zgetrf_incpiv.c.o"
  "CMakeFiles/chameleon.dir/dgetrf_incpiv.c.o"
  "CMakeFiles/chameleon.dir/cgetrf_nopiv.c.o"
  "CMakeFiles/chameleon.dir/sgetrf_nopiv.c.o"
  "CMakeFiles/chameleon.dir/zgetrf_nopiv.c.o"
  "CMakeFiles/chameleon.dir/dgetrf_nopiv.c.o"
  "CMakeFiles/chameleon.dir/cgetrs_incpiv.c.o"
  "CMakeFiles/chameleon.dir/sgetrs_incpiv.c.o"
  "CMakeFiles/chameleon.dir/zgetrs_incpiv.c.o"
  "CMakeFiles/chameleon.dir/dgetrs_incpiv.c.o"
  "CMakeFiles/chameleon.dir/cgetrs_nopiv.c.o"
  "CMakeFiles/chameleon.dir/sgetrs_nopiv.c.o"
  "CMakeFiles/chameleon.dir/zgetrs_nopiv.c.o"
  "CMakeFiles/chameleon.dir/dgetrs_nopiv.c.o"
  "CMakeFiles/chameleon.dir/clacpy.c.o"
  "CMakeFiles/chameleon.dir/slacpy.c.o"
  "CMakeFiles/chameleon.dir/zlacpy.c.o"
  "CMakeFiles/chameleon.dir/dlacpy.c.o"
  "CMakeFiles/chameleon.dir/clange.c.o"
  "CMakeFiles/chameleon.dir/slange.c.o"
  "CMakeFiles/chameleon.dir/zlange.c.o"
  "CMakeFiles/chameleon.dir/dlange.c.o"
  "CMakeFiles/chameleon.dir/clanhe.c.o"
  "CMakeFiles/chameleon.dir/zlanhe.c.o"
  "CMakeFiles/chameleon.dir/clansy.c.o"
  "CMakeFiles/chameleon.dir/slansy.c.o"
  "CMakeFiles/chameleon.dir/zlansy.c.o"
  "CMakeFiles/chameleon.dir/dlansy.c.o"
  "CMakeFiles/chameleon.dir/clantr.c.o"
  "CMakeFiles/chameleon.dir/slantr.c.o"
  "CMakeFiles/chameleon.dir/zlantr.c.o"
  "CMakeFiles/chameleon.dir/dlantr.c.o"
  "CMakeFiles/chameleon.dir/claset.c.o"
  "CMakeFiles/chameleon.dir/slaset.c.o"
  "CMakeFiles/chameleon.dir/zlaset.c.o"
  "CMakeFiles/chameleon.dir/dlaset.c.o"
  "CMakeFiles/chameleon.dir/clauum.c.o"
  "CMakeFiles/chameleon.dir/slauum.c.o"
  "CMakeFiles/chameleon.dir/zlauum.c.o"
  "CMakeFiles/chameleon.dir/dlauum.c.o"
  "CMakeFiles/chameleon.dir/cplghe.c.o"
  "CMakeFiles/chameleon.dir/zplghe.c.o"
  "CMakeFiles/chameleon.dir/cplgsy.c.o"
  "CMakeFiles/chameleon.dir/splgsy.c.o"
  "CMakeFiles/chameleon.dir/zplgsy.c.o"
  "CMakeFiles/chameleon.dir/dplgsy.c.o"
  "CMakeFiles/chameleon.dir/cplrnt.c.o"
  "CMakeFiles/chameleon.dir/splrnt.c.o"
  "CMakeFiles/chameleon.dir/zplrnt.c.o"
  "CMakeFiles/chameleon.dir/dplrnt.c.o"
  "CMakeFiles/chameleon.dir/cposv.c.o"
  "CMakeFiles/chameleon.dir/sposv.c.o"
  "CMakeFiles/chameleon.dir/zposv.c.o"
  "CMakeFiles/chameleon.dir/dposv.c.o"
  "CMakeFiles/chameleon.dir/csysv.c.o"
  "CMakeFiles/chameleon.dir/zsysv.c.o"
  "CMakeFiles/chameleon.dir/cpotrf.c.o"
  "CMakeFiles/chameleon.dir/spotrf.c.o"
  "CMakeFiles/chameleon.dir/zpotrf.c.o"
  "CMakeFiles/chameleon.dir/dpotrf.c.o"
  "CMakeFiles/chameleon.dir/csytrf.c.o"
  "CMakeFiles/chameleon.dir/zsytrf.c.o"
  "CMakeFiles/chameleon.dir/cpotri.c.o"
  "CMakeFiles/chameleon.dir/spotri.c.o"
  "CMakeFiles/chameleon.dir/zpotri.c.o"
  "CMakeFiles/chameleon.dir/dpotri.c.o"
  "CMakeFiles/chameleon.dir/cpotrimm.c.o"
  "CMakeFiles/chameleon.dir/spotrimm.c.o"
  "CMakeFiles/chameleon.dir/zpotrimm.c.o"
  "CMakeFiles/chameleon.dir/dpotrimm.c.o"
  "CMakeFiles/chameleon.dir/cpotrs.c.o"
  "CMakeFiles/chameleon.dir/spotrs.c.o"
  "CMakeFiles/chameleon.dir/zpotrs.c.o"
  "CMakeFiles/chameleon.dir/dpotrs.c.o"
  "CMakeFiles/chameleon.dir/csytrs.c.o"
  "CMakeFiles/chameleon.dir/zsytrs.c.o"
  "CMakeFiles/chameleon.dir/ctrtri.c.o"
  "CMakeFiles/chameleon.dir/strtri.c.o"
  "CMakeFiles/chameleon.dir/ztrtri.c.o"
  "CMakeFiles/chameleon.dir/dtrtri.c.o"
  "CMakeFiles/chameleon.dir/cunglq.c.o"
  "CMakeFiles/chameleon.dir/sorglq.c.o"
  "CMakeFiles/chameleon.dir/zunglq.c.o"
  "CMakeFiles/chameleon.dir/dorglq.c.o"
  "CMakeFiles/chameleon.dir/cungqr.c.o"
  "CMakeFiles/chameleon.dir/sorgqr.c.o"
  "CMakeFiles/chameleon.dir/zungqr.c.o"
  "CMakeFiles/chameleon.dir/dorgqr.c.o"
  "CMakeFiles/chameleon.dir/cunmlq.c.o"
  "CMakeFiles/chameleon.dir/sormlq.c.o"
  "CMakeFiles/chameleon.dir/zunmlq.c.o"
  "CMakeFiles/chameleon.dir/dormlq.c.o"
  "CMakeFiles/chameleon.dir/cunmqr.c.o"
  "CMakeFiles/chameleon.dir/sormqr.c.o"
  "CMakeFiles/chameleon.dir/zunmqr.c.o"
  "CMakeFiles/chameleon.dir/dormqr.c.o"
  "CMakeFiles/chameleon.dir/pctile.c.o"
  "CMakeFiles/chameleon.dir/pstile.c.o"
  "CMakeFiles/chameleon.dir/pztile.c.o"
  "CMakeFiles/chameleon.dir/pdtile.c.o"
  "CMakeFiles/chameleon.dir/ctile.c.o"
  "CMakeFiles/chameleon.dir/stile.c.o"
  "CMakeFiles/chameleon.dir/ztile.c.o"
  "CMakeFiles/chameleon.dir/dtile.c.o"
  "CMakeFiles/chameleon.dir/control/workspace_c.c.o"
  "CMakeFiles/chameleon.dir/control/workspace_s.c.o"
  "CMakeFiles/chameleon.dir/control/workspace_z.c.o"
  "CMakeFiles/chameleon.dir/control/workspace_d.c.o"
  "CMakeFiles/chameleon.dir/control/morse_cf77.c.o"
  "CMakeFiles/chameleon.dir/control/morse_sf77.c.o"
  "CMakeFiles/chameleon.dir/control/morse_zf77.c.o"
  "CMakeFiles/chameleon.dir/control/morse_df77.c.o"
  "libchameleon.pdb"
  "libchameleon.so"
)

# Per-language clean rules from dependency scanning.
foreach(lang C)
  include(CMakeFiles/chameleon.dir/cmake_clean_${lang}.cmake OPTIONAL)
endforeach()
