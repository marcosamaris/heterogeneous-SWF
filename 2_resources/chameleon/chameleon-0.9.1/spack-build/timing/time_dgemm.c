/**
 *
 * @copyright (c) 2009-2014 The University of Tennessee and The University
 *                          of Tennessee Research Foundation.
 *                          All rights reserved.
 * @copyright (c) 2012-2014 Inria. All rights reserved.
 * @copyright (c) 2012-2014 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria, Univ. Bordeaux. All rights reserved.
 *
 **/

/**
 *
 * @generated d Tue Sep 26 20:43:22 2017
 *
 **/
#define _TYPE  double
#define _PREC  double
#define _LAMCH LAPACKE_dlamch_work

#define _NAME  "MORSE_dgemm"
/* See Lawn 41 page 120 */
#define _FMULS FMULS_GEMM(M, N, K)
#define _FADDS FADDS_GEMM(M, N, K)

#include "./timing.c"
#include "timing_dauxiliary.h"

static int
RunTest(int *iparam, double *dparam, morse_time_t *t_) 
{
    double alpha, beta;
    PASTE_CODE_IPARAM_LOCALS( iparam );

    LDB = max(K, iparam[IPARAM_LDB]);
    LDC = max(M, iparam[IPARAM_LDC]);

    /* Allocate Data */
    PASTE_CODE_ALLOCATE_MATRIX( A,      1, double, LDA, K   );
    PASTE_CODE_ALLOCATE_MATRIX( B,      1, double, LDB, N   );
    PASTE_CODE_ALLOCATE_MATRIX( C,      1, double, LDC, N   );
    PASTE_CODE_ALLOCATE_MATRIX( C2, check, double, LDC, N   );

    MORSE_dplrnt( M, K, A, LDA,  453 );
    MORSE_dplrnt( K, N, B, LDB, 5673 );
    MORSE_dplrnt( M, N, C, LDC,  740 );

    LAPACKE_dlarnv_work(1, ISEED, 1, &alpha);
    LAPACKE_dlarnv_work(1, ISEED, 1, &beta );

    if (check)
    {
        memcpy(C2, C, LDC*N*sizeof(double));
    }

    START_TIMING();
    MORSE_dgemm( MorseNoTrans, MorseNoTrans, M, N, K, alpha, A, LDA, B, LDB, beta, C, LDC );
    STOP_TIMING();

    /* Check the solution */
    if (check)
    {
        dparam[IPARAM_RES] = 0.0;
        dparam[IPARAM_RES] = d_check_gemm( MorseNoTrans, MorseNoTrans, M, N, K,
                                           alpha, A, LDA, B, LDB, beta, C, C2, LDC,
                                           &(dparam[IPARAM_ANORM]),
                                           &(dparam[IPARAM_BNORM]),
                                           &(dparam[IPARAM_XNORM]));

        free(C2);
    }

    free( A );
    free( B );
    free( C );

    return 0;
}
