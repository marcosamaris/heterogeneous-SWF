# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.7

# compile C with /home/marcos/chameleon/spack/lib/spack/env/gcc/gcc
C_FLAGS = -O2 -g -DNDEBUG -fPIC  

C_DEFINES = -DADD_ -DCHAMELEON_SCHED_STARPU -DCHAMELEON_USE_MPI -DCHAMELEON_USE_STARPU_DATA_WONT_USE -DCHAMELEON_USE_STARPU_IDLE_PREFETCH -DHAVE_STARPU_MPI_DATA_REGISTER -Dchameleon_starpu_EXPORTS

C_INCLUDES = -I/home/marcos/chameleon/spack/opt/spack/linux-x86_64/gcc-4.8.5/openmpi-2.0.1-wmrycolmrlgieebzmb7vbndworuorggr/include -I/home/marcos/chameleon/spack/opt/spack/linux-x86_64/gcc-4.8.5/starpu-1.2.1-b3ym3ekzbvawzmi5ft7u4wsoomckww6p/include/starpu/1.2 -I/tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1 -I/tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1/spack-build -I/tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1/include -I/tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1/spack-build/include 

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/control/runtime_profiling.c.o_FLAGS =  -DPRECISION_s -DPRECISION_d -DPRECISION_c -DPRECISION_z

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/control/runtime_cprofiling.c.o_FLAGS = -DPRECISION_c

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/control/runtime_sprofiling.c.o_FLAGS = -DPRECISION_s

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/control/runtime_zprofiling.c.o_FLAGS = -DPRECISION_z

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/control/runtime_dprofiling.c.o_FLAGS = -DPRECISION_d

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/control/runtime_clocality.c.o_FLAGS = -DPRECISION_c

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/control/runtime_slocality.c.o_FLAGS = -DPRECISION_s

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/control/runtime_zlocality.c.o_FLAGS = -DPRECISION_z

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/control/runtime_dlocality.c.o_FLAGS = -DPRECISION_d

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_ccallback.c.o_FLAGS = -DPRECISION_c

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_scallback.c.o_FLAGS = -DPRECISION_s

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_zcallback.c.o_FLAGS = -DPRECISION_z

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_dcallback.c.o_FLAGS = -DPRECISION_d

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_ctile_zero.c.o_FLAGS = -DPRECISION_c

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_stile_zero.c.o_FLAGS = -DPRECISION_s

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_ztile_zero.c.o_FLAGS = -DPRECISION_z

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_dtile_zero.c.o_FLAGS = -DPRECISION_d

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_casum.c.o_FLAGS = -DPRECISION_c

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_sasum.c.o_FLAGS = -DPRECISION_s

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_zasum.c.o_FLAGS = -DPRECISION_z

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_dasum.c.o_FLAGS = -DPRECISION_d

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_caxpy.c.o_FLAGS = -DPRECISION_c

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_saxpy.c.o_FLAGS = -DPRECISION_s

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_zaxpy.c.o_FLAGS = -DPRECISION_z

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_daxpy.c.o_FLAGS = -DPRECISION_d

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_cgemm.c.o_FLAGS = -DPRECISION_c

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_sgemm.c.o_FLAGS = -DPRECISION_s

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_zgemm.c.o_FLAGS = -DPRECISION_z

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_dgemm.c.o_FLAGS = -DPRECISION_d

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_chemm.c.o_FLAGS = -DPRECISION_c

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_zhemm.c.o_FLAGS = -DPRECISION_z

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_cher2k.c.o_FLAGS = -DPRECISION_c

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_zher2k.c.o_FLAGS = -DPRECISION_z

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_cherk.c.o_FLAGS = -DPRECISION_c

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_zherk.c.o_FLAGS = -DPRECISION_z

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_csymm.c.o_FLAGS = -DPRECISION_c

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_ssymm.c.o_FLAGS = -DPRECISION_s

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_zsymm.c.o_FLAGS = -DPRECISION_z

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_dsymm.c.o_FLAGS = -DPRECISION_d

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_csyr2k.c.o_FLAGS = -DPRECISION_c

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_ssyr2k.c.o_FLAGS = -DPRECISION_s

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_zsyr2k.c.o_FLAGS = -DPRECISION_z

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_dsyr2k.c.o_FLAGS = -DPRECISION_d

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_csyrk.c.o_FLAGS = -DPRECISION_c

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_ssyrk.c.o_FLAGS = -DPRECISION_s

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_zsyrk.c.o_FLAGS = -DPRECISION_z

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_dsyrk.c.o_FLAGS = -DPRECISION_d

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_ctrmm.c.o_FLAGS = -DPRECISION_c

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_strmm.c.o_FLAGS = -DPRECISION_s

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_ztrmm.c.o_FLAGS = -DPRECISION_z

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_dtrmm.c.o_FLAGS = -DPRECISION_d

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_ctrsm.c.o_FLAGS = -DPRECISION_c

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_strsm.c.o_FLAGS = -DPRECISION_s

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_ztrsm.c.o_FLAGS = -DPRECISION_z

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_dtrsm.c.o_FLAGS = -DPRECISION_d

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_cgeadd.c.o_FLAGS = -DPRECISION_c

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_sgeadd.c.o_FLAGS = -DPRECISION_s

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_zgeadd.c.o_FLAGS = -DPRECISION_z

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_dgeadd.c.o_FLAGS = -DPRECISION_d

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_cgelqt.c.o_FLAGS = -DPRECISION_c

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_sgelqt.c.o_FLAGS = -DPRECISION_s

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_zgelqt.c.o_FLAGS = -DPRECISION_z

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_dgelqt.c.o_FLAGS = -DPRECISION_d

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_cgeqrt.c.o_FLAGS = -DPRECISION_c

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_sgeqrt.c.o_FLAGS = -DPRECISION_s

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_zgeqrt.c.o_FLAGS = -DPRECISION_z

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_dgeqrt.c.o_FLAGS = -DPRECISION_d

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_cgessm.c.o_FLAGS = -DPRECISION_c

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_sgessm.c.o_FLAGS = -DPRECISION_s

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_zgessm.c.o_FLAGS = -DPRECISION_z

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_dgessm.c.o_FLAGS = -DPRECISION_d

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_cgessq.c.o_FLAGS = -DPRECISION_c

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_sgessq.c.o_FLAGS = -DPRECISION_s

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_zgessq.c.o_FLAGS = -DPRECISION_z

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_dgessq.c.o_FLAGS = -DPRECISION_d

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_cgetrf.c.o_FLAGS = -DPRECISION_c

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_sgetrf.c.o_FLAGS = -DPRECISION_s

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_zgetrf.c.o_FLAGS = -DPRECISION_z

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_dgetrf.c.o_FLAGS = -DPRECISION_d

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_cgetrf_incpiv.c.o_FLAGS = -DPRECISION_c

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_sgetrf_incpiv.c.o_FLAGS = -DPRECISION_s

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_zgetrf_incpiv.c.o_FLAGS = -DPRECISION_z

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_dgetrf_incpiv.c.o_FLAGS = -DPRECISION_d

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_cgetrf_nopiv.c.o_FLAGS = -DPRECISION_c

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_sgetrf_nopiv.c.o_FLAGS = -DPRECISION_s

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_zgetrf_nopiv.c.o_FLAGS = -DPRECISION_z

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_dgetrf_nopiv.c.o_FLAGS = -DPRECISION_d

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_chessq.c.o_FLAGS = -DPRECISION_c

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_zhessq.c.o_FLAGS = -DPRECISION_z

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_clacpy.c.o_FLAGS = -DPRECISION_c

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_slacpy.c.o_FLAGS = -DPRECISION_s

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_zlacpy.c.o_FLAGS = -DPRECISION_z

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_dlacpy.c.o_FLAGS = -DPRECISION_d

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_clange.c.o_FLAGS = -DPRECISION_c

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_slange.c.o_FLAGS = -DPRECISION_s

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_zlange.c.o_FLAGS = -DPRECISION_z

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_dlange.c.o_FLAGS = -DPRECISION_d

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_clanhe.c.o_FLAGS = -DPRECISION_c

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_zlanhe.c.o_FLAGS = -DPRECISION_z

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_clansy.c.o_FLAGS = -DPRECISION_c

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_slansy.c.o_FLAGS = -DPRECISION_s

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_zlansy.c.o_FLAGS = -DPRECISION_z

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_dlansy.c.o_FLAGS = -DPRECISION_d

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_clantr.c.o_FLAGS = -DPRECISION_c

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_slantr.c.o_FLAGS = -DPRECISION_s

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_zlantr.c.o_FLAGS = -DPRECISION_z

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_dlantr.c.o_FLAGS = -DPRECISION_d

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_claset2.c.o_FLAGS = -DPRECISION_c

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_slaset2.c.o_FLAGS = -DPRECISION_s

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_zlaset2.c.o_FLAGS = -DPRECISION_z

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_dlaset2.c.o_FLAGS = -DPRECISION_d

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_claset.c.o_FLAGS = -DPRECISION_c

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_slaset.c.o_FLAGS = -DPRECISION_s

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_zlaset.c.o_FLAGS = -DPRECISION_z

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_dlaset.c.o_FLAGS = -DPRECISION_d

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_clauum.c.o_FLAGS = -DPRECISION_c

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_slauum.c.o_FLAGS = -DPRECISION_s

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_zlauum.c.o_FLAGS = -DPRECISION_z

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_dlauum.c.o_FLAGS = -DPRECISION_d

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_cplghe.c.o_FLAGS = -DPRECISION_c

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_zplghe.c.o_FLAGS = -DPRECISION_z

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_cplgsy.c.o_FLAGS = -DPRECISION_c

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_splgsy.c.o_FLAGS = -DPRECISION_s

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_zplgsy.c.o_FLAGS = -DPRECISION_z

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_dplgsy.c.o_FLAGS = -DPRECISION_d

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_cplrnt.c.o_FLAGS = -DPRECISION_c

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_splrnt.c.o_FLAGS = -DPRECISION_s

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_zplrnt.c.o_FLAGS = -DPRECISION_z

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_dplrnt.c.o_FLAGS = -DPRECISION_d

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_cplssq.c.o_FLAGS = -DPRECISION_c

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_splssq.c.o_FLAGS = -DPRECISION_s

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_zplssq.c.o_FLAGS = -DPRECISION_z

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_dplssq.c.o_FLAGS = -DPRECISION_d

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_cpotrf.c.o_FLAGS = -DPRECISION_c

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_spotrf.c.o_FLAGS = -DPRECISION_s

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_zpotrf.c.o_FLAGS = -DPRECISION_z

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_dpotrf.c.o_FLAGS = -DPRECISION_d

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_cssssm.c.o_FLAGS = -DPRECISION_c

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_sssssm.c.o_FLAGS = -DPRECISION_s

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_zssssm.c.o_FLAGS = -DPRECISION_z

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_dssssm.c.o_FLAGS = -DPRECISION_d

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_csyssq.c.o_FLAGS = -DPRECISION_c

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_ssyssq.c.o_FLAGS = -DPRECISION_s

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_zsyssq.c.o_FLAGS = -DPRECISION_z

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_dsyssq.c.o_FLAGS = -DPRECISION_d

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_csytrf_nopiv.c.o_FLAGS = -DPRECISION_c

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_zsytrf_nopiv.c.o_FLAGS = -DPRECISION_z

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_ctrasm.c.o_FLAGS = -DPRECISION_c

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_strasm.c.o_FLAGS = -DPRECISION_s

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_ztrasm.c.o_FLAGS = -DPRECISION_z

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_dtrasm.c.o_FLAGS = -DPRECISION_d

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_ctrssq.c.o_FLAGS = -DPRECISION_c

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_strssq.c.o_FLAGS = -DPRECISION_s

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_ztrssq.c.o_FLAGS = -DPRECISION_z

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_dtrssq.c.o_FLAGS = -DPRECISION_d

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_ctrtri.c.o_FLAGS = -DPRECISION_c

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_strtri.c.o_FLAGS = -DPRECISION_s

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_ztrtri.c.o_FLAGS = -DPRECISION_z

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_dtrtri.c.o_FLAGS = -DPRECISION_d

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_ctslqt.c.o_FLAGS = -DPRECISION_c

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_stslqt.c.o_FLAGS = -DPRECISION_s

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_ztslqt.c.o_FLAGS = -DPRECISION_z

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_dtslqt.c.o_FLAGS = -DPRECISION_d

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_ctsmlq.c.o_FLAGS = -DPRECISION_c

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_stsmlq.c.o_FLAGS = -DPRECISION_s

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_ztsmlq.c.o_FLAGS = -DPRECISION_z

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_dtsmlq.c.o_FLAGS = -DPRECISION_d

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_ctsmqr.c.o_FLAGS = -DPRECISION_c

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_stsmqr.c.o_FLAGS = -DPRECISION_s

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_ztsmqr.c.o_FLAGS = -DPRECISION_z

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_dtsmqr.c.o_FLAGS = -DPRECISION_d

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_ctsqrt.c.o_FLAGS = -DPRECISION_c

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_stsqrt.c.o_FLAGS = -DPRECISION_s

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_ztsqrt.c.o_FLAGS = -DPRECISION_z

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_dtsqrt.c.o_FLAGS = -DPRECISION_d

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_ctstrf.c.o_FLAGS = -DPRECISION_c

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_ststrf.c.o_FLAGS = -DPRECISION_s

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_ztstrf.c.o_FLAGS = -DPRECISION_z

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_dtstrf.c.o_FLAGS = -DPRECISION_d

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_cttlqt.c.o_FLAGS = -DPRECISION_c

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_sttlqt.c.o_FLAGS = -DPRECISION_s

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_zttlqt.c.o_FLAGS = -DPRECISION_z

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_dttlqt.c.o_FLAGS = -DPRECISION_d

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_cttmlq.c.o_FLAGS = -DPRECISION_c

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_sttmlq.c.o_FLAGS = -DPRECISION_s

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_zttmlq.c.o_FLAGS = -DPRECISION_z

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_dttmlq.c.o_FLAGS = -DPRECISION_d

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_cttmqr.c.o_FLAGS = -DPRECISION_c

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_sttmqr.c.o_FLAGS = -DPRECISION_s

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_zttmqr.c.o_FLAGS = -DPRECISION_z

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_dttmqr.c.o_FLAGS = -DPRECISION_d

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_cttqrt.c.o_FLAGS = -DPRECISION_c

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_sttqrt.c.o_FLAGS = -DPRECISION_s

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_zttqrt.c.o_FLAGS = -DPRECISION_z

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_dttqrt.c.o_FLAGS = -DPRECISION_d

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_cunmlq.c.o_FLAGS = -DPRECISION_c

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_sormlq.c.o_FLAGS = -DPRECISION_s

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_zunmlq.c.o_FLAGS = -DPRECISION_z

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_dormlq.c.o_FLAGS = -DPRECISION_d

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_cunmqr.c.o_FLAGS = -DPRECISION_c

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_sormqr.c.o_FLAGS = -DPRECISION_s

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_zunmqr.c.o_FLAGS = -DPRECISION_z

# Custom flags: runtime/starpu/CMakeFiles/chameleon_starpu.dir/codelets/codelet_dormqr.c.o_FLAGS = -DPRECISION_d

