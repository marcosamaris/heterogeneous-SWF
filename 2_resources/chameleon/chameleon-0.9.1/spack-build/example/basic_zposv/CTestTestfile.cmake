# CMake generated Testfile for 
# Source directory: /tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1/example/basic_zposv
# Build directory: /tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1/spack-build/example/basic_zposv
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(example_basic_sposv_morse_functions "./sposv_morse_functions")
add_test(example_basic_sposv_users_functions "./sposv_users_functions")
add_test(example_basic_dposv_morse_functions "./dposv_morse_functions")
add_test(example_basic_dposv_users_functions "./dposv_users_functions")
add_test(example_basic_cposv_morse_functions "./cposv_morse_functions")
add_test(example_basic_cposv_users_functions "./cposv_users_functions")
add_test(example_basic_zposv_morse_functions "./zposv_morse_functions")
add_test(example_basic_zposv_users_functions "./zposv_users_functions")
