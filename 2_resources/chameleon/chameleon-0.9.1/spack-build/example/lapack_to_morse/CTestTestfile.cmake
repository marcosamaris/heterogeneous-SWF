# CMake generated Testfile for 
# Source directory: /tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1/example/lapack_to_morse
# Build directory: /tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1/spack-build/example/lapack_to_morse
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(example_ltm_step0 "./step0")
add_test(example_ltm_step1 "./step1")
add_test(example_ltm_step2 "./step2")
add_test(example_ltm_step3 "./step3")
add_test(example_ltm_step4 "./step4")
add_test(example_ltm_step5 "./step5")
add_test(example_ltm_step6 "./step6")
