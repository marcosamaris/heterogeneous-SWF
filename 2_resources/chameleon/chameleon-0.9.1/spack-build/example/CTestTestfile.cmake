# CMake generated Testfile for 
# Source directory: /tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1/example
# Build directory: /tmp/marcos/spack-stage/spack-stage-fPQgje/chameleon-0.9.1/spack-build/example
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("basic_zposv")
subdirs("lapack_to_morse")
