var modules =
[
    [ "Auxiliary", "group__Auxiliary.html", "group__Auxiliary" ],
    [ "CORE_MORSE_Complex64_t", "group__CORE__MORSE__Complex64__t.html", "group__CORE__MORSE__Complex64__t" ],
    [ "Control", "group__Control.html", "group__Control" ],
    [ "Descriptor", "group__Descriptor.html", "group__Descriptor" ],
    [ "MORSE_Complex64_t", "group__MORSE__Complex64__t.html", "group__MORSE__Complex64__t" ],
    [ "MORSE_Complex64_t_Tile", "group__MORSE__Complex64__t__Tile.html", "group__MORSE__Complex64__t__Tile" ],
    [ "MORSE_Complex64_t_Tile_Async", "group__MORSE__Complex64__t__Tile__Async.html", "group__MORSE__Complex64__t__Tile__Async" ],
    [ "Options", "group__Options.html", "group__Options" ],
    [ "Sequences", "group__Sequences.html", "group__Sequences" ],
    [ "Tile", "group__Tile.html", "group__Tile" ],
    [ "Workspace", "group__Workspace.html", "group__Workspace" ]
];