var descriptor_8c =
[
    [ "MORSE_Desc_Acquire", "group__Descriptor_ga168297afe60944d863cb95ac1a71bc0d.html#ga168297afe60944d863cb95ac1a71bc0d", null ],
    [ "morse_desc_check", "descriptor_8c.html#ad7bcbda3e605190ee0798d3948f60026", null ],
    [ "MORSE_Desc_Create", "group__Descriptor_ga736c6f03606c0aafdcf78020e14e0d1e.html#ga736c6f03606c0aafdcf78020e14e0d1e", null ],
    [ "MORSE_Desc_Create_User", "group__Descriptor_gabe533832d808b3e44a53c39a6b954573.html#gabe533832d808b3e44a53c39a6b954573", null ],
    [ "MORSE_Desc_Destroy", "group__Descriptor_gacb0215d688a1f1086554491ba23e95b9.html#gacb0215d688a1f1086554491ba23e95b9", null ],
    [ "MORSE_Desc_Getoncpu", "group__Descriptor_ga56b3933d6115489af106629cd96b4089.html#ga56b3933d6115489af106629cd96b4089", null ],
    [ "morse_desc_init", "descriptor_8c.html#a237e08355ae42d25f0b4f386e8db5b76", null ],
    [ "morse_desc_init_user", "descriptor_8c.html#acda07e7fb4b2bd07cb8588bcf1a17798", null ],
    [ "morse_desc_mat_alloc", "descriptor_8c.html#ad0ad7392d34d3388bc2a90d72292e142", null ],
    [ "morse_desc_mat_free", "descriptor_8c.html#abe363650691d2da5ac0259575ac91f1a", null ],
    [ "MORSE_Desc_Release", "group__Descriptor_gac0984670481bcd06feee6a303fce2d18.html#gac0984670481bcd06feee6a303fce2d18", null ],
    [ "morse_desc_submatrix", "descriptor_8c.html#a08cefe82f867b7d0594a5601f7b672a3", null ],
    [ "nbdesc", "descriptor_8c.html#a31d5a8aedee25b1cd885367ab0013b9d", null ]
];