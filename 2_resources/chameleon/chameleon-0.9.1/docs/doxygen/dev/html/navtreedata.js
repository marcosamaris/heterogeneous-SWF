var NAVTREE =
[
  [ "CHAMELEON", "index.html", [
    [ "Chameleon, a dense linear algebra library for scalable multi-core architectures and GPGPUs", "index.html", [
      [ "Auxiliary and control routines", "index.html#sec_auxiliary", null ],
      [ "Linear algebra routines", "index.html#sec_linalg", null ]
    ] ],
    [ "Modules", "modules.html", "modules" ],
    [ "Files", null, [
      [ "File List", "files.html", "files" ],
      [ "Globals", "globals.html", [
        [ "All", "globals.html", "globals_dup" ],
        [ "Functions/Subroutines", "globals_func.html", "globals_func" ],
        [ "Variables", "globals_vars.html", null ],
        [ "Macros", "globals_defs.html", null ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"async_8c.html",
"core__zherk_8c_source.html",
"group__CORE__MORSE__Complex64__t_ga99234a6e26b6ae4018751acccef5f9af.html#ga99234a6e26b6ae4018751acccef5f9af",
"group__MORSE__Complex64__t_ga283cf45f166d44e7b7e83dcf7f71b002.html#ga283cf45f166d44e7b7e83dcf7f71b002",
"morse__zf77_8c.html#a460ed8e623ae8f2be045501bb9150283",
"pzgeqrf_8c_source.html",
"runtime__options_8c.html#a1c5e3ae19e8a5a48c5741a132b433584",
"simulapacke_8c.html#a07c69b38065aee1fcda2fc533f4e3c26",
"simulapacke_8c.html#a1c0a76320e594b04fd384877385d7b71",
"simulapacke_8c.html#a320113eb44f0d2120197690d2dca8da2",
"simulapacke_8c.html#a4629d5ef23cb18ef87abe148708e7dad",
"simulapacke_8c.html#a5b4f25975e01ef6fef28959cca364904",
"simulapacke_8c.html#a6fe2a100c43ce81b43ae242f9763e3ec",
"simulapacke_8c.html#a83506491ea0ebab1af82a552dd80b781",
"simulapacke_8c.html#a95b6761e00b628025909e25aaa0d5690",
"simulapacke_8c.html#aaa3dd9562a14db071283cbdf579f1f00",
"simulapacke_8c.html#ac089b8d07c9c687be7c62c766e1e5145",
"simulapacke_8c.html#ad496e66bbb35f7a9fcc929df15a838d1",
"simulapacke_8c.html#ae920304cb549bb68d5eec1115df65747",
"simulapacke_8c.html#afc40895ea83e96568c61dcb0fb10c3f3"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';