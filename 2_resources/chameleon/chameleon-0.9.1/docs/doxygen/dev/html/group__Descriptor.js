var group__Descriptor =
[
    [ "MORSE_Desc_Acquire", "group__Descriptor_ga168297afe60944d863cb95ac1a71bc0d.html#ga168297afe60944d863cb95ac1a71bc0d", null ],
    [ "MORSE_Desc_Create", "group__Descriptor_ga736c6f03606c0aafdcf78020e14e0d1e.html#ga736c6f03606c0aafdcf78020e14e0d1e", null ],
    [ "MORSE_Desc_Create_User", "group__Descriptor_gabe533832d808b3e44a53c39a6b954573.html#gabe533832d808b3e44a53c39a6b954573", null ],
    [ "MORSE_Desc_Destroy", "group__Descriptor_gacb0215d688a1f1086554491ba23e95b9.html#gacb0215d688a1f1086554491ba23e95b9", null ],
    [ "MORSE_Desc_Getoncpu", "group__Descriptor_ga56b3933d6115489af106629cd96b4089.html#ga56b3933d6115489af106629cd96b4089", null ],
    [ "MORSE_Desc_Release", "group__Descriptor_gac0984670481bcd06feee6a303fce2d18.html#gac0984670481bcd06feee6a303fce2d18", null ]
];