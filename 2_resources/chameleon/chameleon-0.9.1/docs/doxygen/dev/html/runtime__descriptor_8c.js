var runtime__descriptor_8c =
[
    [ "RUNTIME_desc_acquire", "runtime__descriptor_8c.html#ab56c4de4e9da126b42341e1cfce3ddc7", null ],
    [ "RUNTIME_desc_create", "runtime__descriptor_8c.html#a1aa0f34f8be533ab4b1e23213f8ae536", null ],
    [ "RUNTIME_desc_destroy", "runtime__descriptor_8c.html#a0d23abb33d6248b0c97e96f94fcebded", null ],
    [ "RUNTIME_desc_getaddr", "runtime__descriptor_8c.html#a4a472c48a34af29d73a13d0a4fc305c1", null ],
    [ "RUNTIME_desc_getoncpu", "runtime__descriptor_8c.html#addaadda2133995a292b8340d0d92750c", null ],
    [ "RUNTIME_desc_init", "runtime__descriptor_8c.html#a56cfbffdbf7f29393bfa3266082b9a6a", null ],
    [ "RUNTIME_desc_release", "runtime__descriptor_8c.html#acca3e26e2298a01856135bdd232ee6f0", null ],
    [ "RUNTIME_desc_submatrix", "runtime__descriptor_8c.html#a61e4d15c39efd28dc7db92571b0ca042", null ]
];