var morse__mf77_8c =
[
    [ "MORSE_DSGELS", "morse__mf77_8c.html#a7d6d4eea682d71fa3662893873f16518", null ],
    [ "MORSE_DSGELS_TILE", "morse__mf77_8c.html#a55d9777b52cee892c22b05cea34f2e38", null ],
    [ "MORSE_DSGESV", "morse__mf77_8c.html#a6dca96692b689ae7442cce65e250fcb0", null ],
    [ "MORSE_DSGESV_TILE", "morse__mf77_8c.html#a231c23204c949394d03788780a88cac3", null ],
    [ "MORSE_DSPOSV", "morse__mf77_8c.html#adfea4adea6e8b1c4f5d6aa6035fc1f03", null ],
    [ "MORSE_DSPOSV_TILE", "morse__mf77_8c.html#a931b6046078da49cffaf5b12495b42b8", null ],
    [ "MORSE_DSUNGESV", "morse__mf77_8c.html#a30c51486799554fffd1f1abf0a439a2c", null ],
    [ "MORSE_DSUNGESV_TILE", "morse__mf77_8c.html#a78bba0e0db15f969eb0142f28f69260b", null ],
    [ "MORSE_ZCGELS", "morse__mf77_8c.html#ac936b775e6934eb0fc46c8ce2b5d8878", null ],
    [ "MORSE_ZCGELS_TILE", "morse__mf77_8c.html#ab947624b3cb81109f3ba6e23e9b23274", null ],
    [ "MORSE_ZCGESV", "morse__mf77_8c.html#a588a2752c95df1330d47d0e1c816252f", null ],
    [ "MORSE_ZCGESV_TILE", "morse__mf77_8c.html#a3ec987647ee23bdf4906d9b698c2908b", null ],
    [ "MORSE_ZCPOSV", "morse__mf77_8c.html#ae383919048725068a664f618d1337f75", null ],
    [ "MORSE_ZCPOSV_TILE", "morse__mf77_8c.html#a1512eeca01794bba63ac1ea6754e20e1", null ],
    [ "MORSE_ZCUNGESV", "morse__mf77_8c.html#ac80b22c58c6651ba4fc93e7438fa16f6", null ],
    [ "MORSE_ZCUNGESV_TILE", "morse__mf77_8c.html#ac02dbc5634fd1fe240f5c8c21c422202", null ]
];