var morse__f77_8c =
[
    [ "MORSE_DEALLOC_HANDLE", "morse__f77_8c.html#a4ee6507ba7c14ddc5b80a736b9d3d906", null ],
    [ "MORSE_DESC_CREATE", "morse__f77_8c_a2751c9300c525d48093e1bf4d8f2592c.html#a2751c9300c525d48093e1bf4d8f2592c", null ],
    [ "MORSE_DESC_DESTROY", "morse__f77_8c.html#a12161424fe5ce4568e04f117e38e2529", null ],
    [ "MORSE_DISABLE", "morse__f77_8c.html#a4c5f9300b38a0c741161fc0654284e01", null ],
    [ "MORSE_ENABLE", "morse__f77_8c.html#a53a8054f854e3fa031979307261c9c16", null ],
    [ "MORSE_FINALIZE", "morse__f77_8c.html#a6fb039d88bfd334ba8497bedd9cbb45b", null ],
    [ "MORSE_GET", "morse__f77_8c.html#a9b407a2a580dd69842bfeb5836b582c3", null ],
    [ "MORSE_INIT", "morse__f77_8c.html#a8c9d15b5114f14c62afe05e074f0d6e1", null ],
    [ "MORSE_LAPACK_TO_TILE", "morse__f77_8c_a19e83e9b17c133ce0fae1cd84c0f02f7.html#a19e83e9b17c133ce0fae1cd84c0f02f7", null ],
    [ "MORSE_SET", "morse__f77_8c.html#a3e70cc367b3ddb4f7768dc1b010dab92", null ],
    [ "MORSE_TILE_TO_LAPACK", "morse__f77_8c.html#a4b3d6e97272086632b91630779e79466", null ],
    [ "MORSE_VERSION", "morse__f77_8c.html#af581cf223a5d9a6848888961bb703834", null ]
];